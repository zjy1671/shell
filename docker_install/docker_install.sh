#!/bin/bash
clear && echo "
##################################################################
#                                                                #                   
#                Install The Docker Precondition                 #      
#                                                                #   
# 一、可以与外网通信。                                           #  
# 二、关闭防火墙和SElinux。                                      #  
# 三、安装了wget 命令，wget获取阿里源镜像                        #
# 四、如果有阿里源镜像，可以直接拉入到 /etc/yum.repos.d/ 目录下  #
#                                                                #
##################################################################
"
while true;do
read -p "如果符合以上，是否进行安装Docker(y/n or Y/N)" choose
case $choose in
	[yY])
		clear
		echo "
#######################################
#                                     #
#     Starting Install The Docker     #
#                                     #
#######################################
"
		echo 
		break
		;;
	[nN])
		exit
		;;
	*)
		echo "请输入(y/n or Y/N)"
		sleep 1.5
		continue
esac
done
###备份yum.repos.d yum文件
tar zcvf /root/repos.tgz /etc/yum.repos.d/*
rm -rf /etc/yum.repos.d/*
###获取阿里镜像源
echo "1.正在获取阿里镜像源"
rpm -qa | grep wget &>/dev/null
if [ $? -eq 0 ];then
	wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo &>/dev/null && wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo && wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo &>/dev/null
	echo "OK"
	echo
else
	yum -y install wget &>/dev/null && wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo &>/dev/null && wget -O /etc/yum.repos.d/epel.repo &>/dev/null
	echo "OK"
	echo
fi

###重新构建yum 缓存
echo "2.正在重新构建yum 缓存"
yum clean all &>/dev/null && yum makecache &>/dev/null
if [ $? -eq 0 ];then
	echo "OK"
	echo
else
	echo "NO"
	echo
fi

###卸载旧版本的Docker
echo "3.正在卸载旧版本的Docker"
yum -y remove docker docker-common docker-selinux docker-engine &>/dev/null
if [ $? -eq 0 ];then
        echo "OK"
        echo
else
        echo "NO"
        echo
fi

###安装Docker 时需要的软件包
echo "4.正在安装Docker 的依赖包"
yum install -y yum-utils device-mapper-persistent-data lvm2a &>/dev/null
if [ $? -eq 0 ];then
        echo "OK"
        echo
else
        echo "NO"
        echo
fi

###获取Docker 的镜像源
echo "5.正在获取Docker 的镜像源"
wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo  && yum clean all && yum makecache
if [ $? -eq 0 ];then
	echo "OK"
	echo
else
	echo "NO"
	echo
fi

###把软件仓库地址替换为 TUNA
echo "6.替换软件仓库地址"
sed -i 's+download.docker.com+mirrors.tuna.tsinghua.edu.cn/docker-ce+' /etc/yum.repos.d/docker-ce.repo
if [ $? -eq 0 ];then
        echo "OK"
        echo
else
        echo "NO"
        echo
fi

###安装Docker
echo "7.开始yum 安装Docker"
yum -y install docker-ce &>/dev/null
if [ $? -eq 0 ];then
        echo "OK"
        echo
else
        echo "NO"
        echo
fi

###设置Docker 开机自启动
systemctl start docker && systemctl enable docker &>/dev/null
if [ $? -eq 0 ];then
        echo "OK"
        echo
else
        echo "NO"
        echo
fi

###配置docker 国内镜像源
echo "8.配置docker 国内镜像源"
/usr/bin/ls /etc/ | grep -w "docker" && /usr/bin/ls /etc/docker | grep -w "daemon.json"
if [ $? -eq 0 ];then 
cat /etc/docker/daemon.json << EOF
{
 
"registry-mirrors": ["https://registry.docker-cn.com"]
 
}
EOF
	systemctl restart docker
	echo "OK"
	echo
else
	mkdir -p /etc/docker &>/dev/null && touch /etc/docker/daemon.json
cat > /etc/docker/daemon.json << EOF
{
 
"registry-mirrors": ["https://registry.docker-cn.com"]
 
}
EOF
	systemctl restart docker
	echo "OK"
fi

###查看Docker 版本
echo "9.当前Docker 版本"
docker -v
echo

###运行第一个Docker容器
echo "10.运行第一个Docker容器"
docker run hello-world
echo
echo "
Hellow,Thank you for using install_docker.sh!
The Docker Installation Is Complete.
Look forward to our next meeting."
echo

##Docker容器自动补全
yum -y install bash-completion
echo "执行此命令使其生效：source /usr/share/bash-completion/bash_completion"