#!/bin/bash
useradd -M -s /sbin/nologin mysql &>/dev/null
touch /var/log/mysql_install_`date +%F_%Hh`.log

file=`ls -t /var/log/mysql_install*.log |& head -n 1`
if [[ -f $file ]];then
        echo > $file
fi

echo "MySQL 5.x.x 可安装版本:
（1）MySQL Version：5.5.8	    （37）MySQL Version：5.5.44	        （73）MySQL Version：5.6.18	    （109）MySQL Version：5.7.3-m13
（2）MySQL Version：5.5.9	    （38）MySQL Version：5.5.45	        （74）MySQL Version：5.6.19	    （110）MySQL Version：5.7.4-m14
（3）MySQL Version：5.5.10	    （39）MySQL Version：5.5.46	        （75）MySQL Version：5.6.20	    （111）MySQL Version：5.7.5-m15
（4）MySQL Version：5.5.11	    （40）MySQL Version：5.5.47	        （76）MySQL Version：5.6.21	    （112）MySQL Version：5.7.6-m16
（5）MySQL Version：5.5.12	    （41）MySQL Version：5.5.48	        （77）MySQL Version：5.6.22	    （113）MySQL Version：5.7.7-rc
（6）MySQL Version：5.5.13	    （42）MySQL Version：5.5.49	        （78）MySQL Version：5.6.23	    （114）MySQL Version：5.7.8-rc
（7）MySQL Version：5.5.14	    （43）MySQL Version：5.5.50	        （79）MySQL Version：5.6.24	    （115）MySQL Version：5.7.9
（8）MySQL Version：5.5.15	    （44）MySQL Version：5.5.51	        （80）MySQL Version：5.6.25	    （116）MySQL Version：5.7.10
（9）MySQL Version：5.5.16	    （45）MySQL Version：5.5.52	        （81）MySQL Version：5.6.26	    （117）MySQL Version：5.7.11
（10）MySQL Version：5.5.17	    （46）MySQL Version：5.5.53	        （82）MySQL Version：5.6.27	    （118）MySQL Version：5.7.12
（11）MySQL Version：5.5.18	    （47）MySQL Version：5.5.54	        （83）MySQL Version：5.6.28	    （119）MySQL Version：5.7.13
（12）MySQL Version：5.5.19	    （48）MySQL Version：5.5.55	        （84）MySQL Version：5.6.29	    （120）MySQL Version：5.7.14
（13）MySQL Version：5.5.20	    （49）MySQL Version：5.5.56	        （85）MySQL Version：5.6.30	    （121）MySQL Version：5.7.15
（14）MySQL Version：5.5.21	    （50）MySQL Version：5.5.57	        （86）MySQL Version：5.6.31	    （122）MySQL Version：5.7.16
（15）MySQL Version：5.5.22	    （51）MySQL Version：5.5.58	        （87）MySQL Version：5.6.32	    （123）MySQL Version：5.7.17
（16）MySQL Version：5.5.23	    （52）MySQL Version：5.5.59	        （88）MySQL Version：5.6.33	    （124）MySQL Version：5.7.18
（17）MySQL Version：5.5.24	    （53）MySQL Version：5.5.60	        （89）MySQL Version：5.6.34	    （125）MySQL Version：5.7.19
（18）MySQL Version：5.5.25a	    （54）MySQL Version：5.5.61	        （90）MySQL Version：5.6.35	    （126）MySQL Version：5.7.20
（19）MySQL Version：5.5.26	    （55）MySQL Version：5.5.62	        （91）MySQL Version：5.6.36	    （127）MySQL Version：5.7.21
（20）MySQL Version：5.5.27	    （56）MySQL Version：5.5.63	        （92）MySQL Version：5.6.37	    （128）MySQL Version：5.7.22
（21）MySQL Version：5.5.28	    （57）MySQL Version：5.6.2-m5	（93）MySQL Version：5.6.38	    （129）MySQL Version：5.7.23
（22）MySQL Version：5.5.29	    （58）MySQL Version：5.6.3-m6	（94）MySQL Version：5.6.39	    （130）MySQL Version：5.7.24
（23）MySQL Version：5.5.30	    （59）MySQL Version：5.6.4-m7	（95）MySQL Version：5.6.40	    （131）MySQL Version：5.7.25
（24）MySQL Version：5.5.31	    （60）MySQL Version：5.6.5-m8	（96）MySQL Version：5.6.41	    （132）MySQL Version：5.7.26
（25）MySQL Version：5.5.32	    （61）MySQL Version：5.6.6-m9	（97）MySQL Version：5.6.42	    （133）MySQL Version：5.7.27
（26）MySQL Version：5.5.33	    （62）MySQL Version：5.6.7-rc	（98）MySQL Version：5.6.43	    （134）MySQL Version：5.7.28
（27）MySQL Version：5.5.34	    （63）MySQL Version：5.6.8-rc	（99）MySQL Version：5.6.44	    （135）MySQL Version：5.7.29
（28）MySQL Version：5.5.35	    （64）MySQL Version：5.6.9-rc	（100）MySQL Version：5.6.45	    （136）MySQL Version：5.7.30
（29）MySQL Version：5.5.36	    （65）MySQL Version：5.6.10	        （101）MySQL Version：5.6.46	    （137）MySQL Version：5.7.31
（30）MySQL Version：5.5.37	    （66）MySQL Version：5.6.11	        （102）MySQL Version：5.6.47	    （138）MySQL Version：5.7.32
（31）MySQL Version：5.5.38	    （67）MySQL Version：5.6.12	        （103）MySQL Version：5.6.48	    （139）MySQL Version：5.7.33
（32）MySQL Version：5.5.39	    （68）MySQL Version：5.6.13	        （104）MySQL Version：5.6.49	    （140）MySQL Version：5.7.34
（33）MySQL Version：5.5.40	    （69）MySQL Version：5.6.14	        （105）MySQL Version：5.6.50	    （141）MySQL Version：5.7.35
（34）MySQL Version：5.5.41	    （70）MySQL Version：5.6.15	        （106）MySQL Version：5.6.51	    （142）MySQL Version：5.7.36
（35）MySQL Version：5.5.42	    （71）MySQL Version：5.6.16	        （107）MySQL Version：5.7.1-m11	    （143）MySQL Version：5.7.37
（36）MySQL Version：5.5.43	    （72）MySQL Version：5.6.17	        （108）MySQL Version：5.7.2-m12	    （144）MySQL Version：5.7.38
"
echo "安装时可查看：tail -f /var/log/mysql_install_`date +%F_%Hh`.log 日志"
echo
read -p "请输入安装MySQL的版本：" choose
echo

echo "一、查看是否安装了mariadb"
echo
rpm -qa |grep mariadb-server &>>/var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then
        echo -e "\033[31m Mariadb has been installed please try uninstalling it, otherwise MySQL cannot be installed ！！！ \033[0m"
else
        echo -e "\033[32m Not Detected MariaDB ！！！ \033[0m"
fi
echo

echo "二、正在下载MySQL 源码包"
echo
wget -c https://downloads.mysql.com/archives/get/p/23/file/mysql-"$choose".tar.gz -P /usr/local/src/ &>>/var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then
	echo -e "\033[32m Download Successfully ！！！\033[0m"
else
	echo -e "\033[31m Download Failed ！！！\033[0m"
	exit
fi
echo

echo "三、正在安装MySQL 编译安装时需要安装的依赖"
echo
yum install -y cmake ncurses ncurses-devel libarchive gcc gcc-c++ openssl openssl-devel libtirpc libtirpc-devel &>>/var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then
	echo -e "\033[32m Install Succeed ！！！\033[0m"
else
	echo -e "\033[31m Install Error ！！！\033[0m"
        echo -e "\033[31m Please check the logs：/var/log/mysql_install_`date +%F_%Hh`.log \033[0m"
	exit
fi
echo 

echo "四、创建MySQL 需要的目录"
echo
mkdir -p /usr/local/mysql/sock/ && mkdir -p /usr/local/mysql/log/ && mkdir -p /usr/local/mysql/mysql-bin-log/ && mkdir -p /usr/local/mysql/pid/ && chown mysql:mysql /usr/local/mysql/* -R && chown mysql:mysql /usr/local/mysql
if [ $? -eq 0 ];then
        echo -e "\033[32m Mkdir Succeed ！！！\033[0m"
else
        echo -e "\033[31m Mkdir Error ！！！\033[0m"
        echo -e "\033[31m Please check the logs：/var/log/mysql_install_`date +%F_%Hh`.log \033[0m"
	exit
fi
echo

echo "五、编译安装MySQL"
echo
tar xvf /usr/local/src/mysql-"$choose".tar.gz -C /usr/local/src/ &>>/var/log/mysql_install_`date +%F_%Hh`.log && cd /usr/local/src/mysql-$choose/ &>>/var/log/mysql_install_`date +%F_%Hh`.log

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DMYSQL_UNIX_ADDR=/usr/local/mysql/sock/mysql.sock -DDEFAULT_CHARSET=utf8mb4 -DDEFAULT_COLLATION=utf8_general_ci -DEXTRA_CHARSETS=all -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_MEMORY_STORAGE_ENGINE=1 -DWITH_READLINE=1 -DWITH_ZLIB=system -DWITH_SSL=system -DWITH_ZLIB=system -DWITH_LIBWRAP=0 -DENABLED_LOCAL_INFILE=1 -DWITH_EMBEDDED_SERVER=1 -DWITH_DEBUG=0 -DMYSQL_DATADIR=/usr/local/mysql/data -DMYSQL_USER=mysql -DMYSQL_TCP_PORT=3306 &>>/var/log/mysql_install_`date +%F_%Hh`.log && make &>>/var/log/mysql_install_`date +%F_%Hh`.log && make install &>>/var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then
	echo -e "\033[32m Install Succeed ！！！\033[0m"
else
	echo -e "\033[31m Install Error ！！！\033[0m"
	echo -e "\033[31m Please check the logs：/var/log/mysql_install_`date +%F_%Hh`.log \033[0m"
	exit
fi
echo

#mysql 配置文件
fun_my_cnf(){
cat << EOF > /etc/my.cnf
[mysqld]
# 使用 xtrabackup-v2 必须制定datadir, 因为操作可能直接对 datadir进行
datadir=/usr/local/mysql/data

#[mysqld3306]

# 为PXC定制
binlog_format=ROW
default-storage-engine         = InnoDB
innodb_large_prefix=on
innodb_autoinc_lock_mode=2
character_set_server=utf8

# 绑定地址之后只能通过内部网络访问
# 不能使用: 10.*.*.*
bind-address=127.0.0.1

auto-increment-increment = 2
auto-increment-offset = 1

# 使用 xtrabackup-v2 必须制定datadir, 因为操作可能直接对 datadir进行
datadir=/usr/local/mysql/data

# http://www.percona.com/doc/percona-xtrabackup/2.2/innobackupex/privileges.html#permissions-and-privileges-needed
# 权限的配置
# xtrapbackup在Donor上执行，因此只需localhost的权限



# 约定:
server-id = 1


# wsrep模式不依赖于GTID
# 开启GTID
# enforce_gtid_consistency=1
# gtid_mode=on

# 即便临时作为Slave，也记录自己的binlog
log-slave-updates=1

# master_info_repository=TABLE
# relay_log_info_repository=TABLE

# GENERAL #
user                           = mysql
port                           = 3306
socket                         = /usr/local/mysql/sock/mysql.sock
pid-file                       = /usr/local/mysql/pid/mysql.pid

# MyISAM #
key-buffer-size                = 32M
myisam-recover                 = FORCE,BACKUP


ft-min-word-len = 4
event-scheduler = 0

# SAFETY #
max-allowed-packet             = 16M

skip-name-resolve
max_connections = 2000
max_connect_errors = 10

back-log = 500

character-set-client-handshake = 1
character-set-server = utf8mb4
character-set-client = utf8mb4
collation-server = utf8mb4_unicode_ci

#character-set-client-handshake=1
#character-set-client=utf8
#character-set-server=utf8
#collation-server=utf8_general_ci



#key-buffer-size = 256M
table-open-cache = 2048
max-allowed-packet = 2048M
slave-skip-errors = all                       #Skip duplicated key
sort-buffer-size = 4M
join-buffer-size = 8M
thread-cache-size = 50
concurrent-insert = 2

thread-stack = 192K
net-buffer-length = 8K
read-buffer-size = 256K
read-rnd-buffer-size = 16M
bulk-insert-buffer-size = 64M

# 采用thread pool来处理连接
#thread-handling=pool-of-threads
# 新线程创建等待时间，, 单位为10ms，50即为500ms
#thread-pool-stall-limit = 50


sql-mode                       = STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_AUTO_VALUE_ON_ZERO,NO_ENGINE_SUBSTITUTION
sysdate-is-now                 = 1
innodb                         = FORCE
innodb-strict-mode             = 1


# BINARY LOGGING #
log-bin                        = /usr/local/mysql/mysql-bin-log/mysql-bin
expire-logs-days               = 5
# LOGGING #
# log-output=file 默认就是FILE
log-error                      = /usr/local/mysql/log/mysql-error.log
long-query-time = 0.3
# log-queries-not-using-indexes  = 1
slow-query-log                 = 1
slow-query-log-file            = /usr/local/mysql/log/mysql-slow.log

# 默认为0(MySQL不控制binlog的输出)
# sync-binlog                    = 1

# CACHES AND LIMITS #

tmp-table-size                 = 32M
max-heap-table-size            = 32M

# 频繁修改的表不适合做query-cache, 否则反而影响效率
query-cache-type               = 0
query-cache-size               = 0
# query-cache-limit = 2M
# query-cache-min-res-unit = 512

thread-cache-size              = 100
open-files-limit               = 65535
table-definition-cache         = 1024
table-open-cache               = 4096

# INNODB #
innodb-flush-method            = O_DIRECT
innodb-log-files-in-group      = 2

# innodb-file-per-table = 1设置之后， 下面的配置基本失效
innodb_data_file_path = ibdata1:10M:autoextend
innodb-thread-concurrency = 32
innodb-log-file-size           = 256M
innodb-flush-log-at-trx-commit = 2
innodb-file-per-table          = 1
innodb-large-prefix            = on
# 内存: 全部内存*0.7
#innodb-buffer-pool-size = 8G

performance-schema = 0
net-read-timeout = 60

# innodb-open-files 在MySQL5.6 auto-sized
# 来自May2
innodb-rollback-on-timeout
innodb-status-file = 1


# http://dev.mysql.com/doc/refman/5.6/en/innodb-performance-multiple_io_threads.html
# http://zhan.renren.com/formysql?tagId=3942&checked=true
# 从MySQL 5.5
# innodb_file_io_threads = 4
innodb-read-io-threads = 16
innodb-write-io-threads = 8

innodb-io-capacity = 2000
# innodb-stats-update-need-lock = 0 # MySQL 5.6中无效
# innodb-stats-auto-update = 0
innodb-old-blocks-pct = 75
# innodb-adaptive-flushing-method = "estimate"
# innodb-adaptive-flushing = 1

# https://www.facebook.com/notes/mysql-at-facebook/repeatable-read-versus-read-committed-for-innodb/244956410932
# READ-COMMITTED 每次QUERY都要求调用: read_view_open_now, 而REPEATABLE-READ每次Transaction中只要求一次
# REPEATABLE-READ 会导致读写不同步
transaction-isolation = READ-COMMITTED

innodb-sync-spin-loops = 100
innodb-spin-wait-delay = 30




innodb-file-format = "Barracuda"
innodb-file-format-max = "Barracuda"


wait_timeout=28800
interactive_timeout=1800

# 等保3，登陆控制
# https://www.cnblogs.com/zhenxing/p/11050823.html
#plugin-load-add                                 = connection_control.so
#connection-control                              = FORCE
#connection-control-failed-login-attempts        = FORCE
# 响应延迟的最小时间，默认30000微秒，30秒
#connection_control_min_connection_delay         = 300000
# 响应延迟的最大时间，默认约24天
#connection_control_max_connection_delay         = 2147483647
# 失败尝试的次数，默认为5，表示当连接失败3次后启用连接控制，0表示不开启
#connection_control_failed_connections_threshold = 5

# 等保3，密码验证要求
# http://dev.mysql.com/doc/refman/5.6/en/validate-password-plugin.html#validate-password-plugin-installation
# https://blog.csdn.net/wltsysterm/article/details/79649484
#plugin-load-add                                 = validate_password.so

# validate-password=ON/OFF/FORCE/FORCE_PLUS_PERMANENT: 决定是否使用该插件(及强制/永久强制使用)。
#validate-password=FORCE_PLUS_PERMANENT
# 密码最小长度
#validate_password_length                        = 8
# 密码至少要包含的小写字母个数和大写字母个数
#validate_password_mixed_case_count              = 1
# 密码至少要包含的数字个数
#validate_password_number_count                  = 1
# 密码至少要包含的特殊字符数
#validate_password_special_char_count            = 1
# 密码强度检查等级，0/LOW:只检查长度、1/MEDIUM:检查长度、数字、大小写、特殊字符、2/STRONG:2/STRONG：检查长度、数字、大小写、特殊字符字典文件。
#validate_password_policy                        = 1

#plugin-load-add                                 = audit_log.so
#audit-log=FORCE_PLUS_PERMANENT


local_infile=off
# have_symlink=off
symbolic-links=off
# 设置general log日志
# https://blog.csdn.net/szgyunyun/article/details/121993852
# 开启general log日志
general-log = 1
# 定义general log日志的位置
general_log_file = /usr/local/mysql/log/mysql_general.log
EOF
}

if [ -f /etc/my.cnf ];then
	cp /etc/my.cnf /etc/my.cnf_`date +%F_%Hh`
	fun_my_cnf
	chown mysql:mysql /etc/my.cnf
else
	fun_my_cnf
	chown mysql:mysql /etc/my.cnf
fi

echo "六、将MySQL 目录链接到/usr/bin/ 目录下"
echo
ln -sf /usr/local/mysql/bin/* /usr/bin/ &>>/var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then
	echo -e "\033[32m Link To Success ！！！\033[0m"
else
	echo -e "\033[31m Link To Error ！！！\033[0m"
	exit
fi
echo 

echo "七、通过Systemctl 管理MySQL"
if [ -f /usr/lib/systemd/system/mysqld.service ];then
	echo -e "\033[31m Error ！！！ mysqld.service 文件已经存在 \033[0m"
else
cat << EOF > /usr/lib/systemd/system/mysqld.service
[Unit]
#当前配置文件的描述信息
Description=MySQL

#表示当前服务是在那个服务后面启动，一般定义为网络服务启动后启动
After=network.target    

[Service] 
#定义启动类型                              
Type=forking         

#重启服务时执行的命令
ExecStartPre=/usr/local/mysql/support-files/mysql.server stop
ExecStartPre=/usr/local/mysql/support-files/mysql.server start

#启动服务时执行的命
ExecStart=/usr/local/mysql/support-files/mysql.server start

#重载服务时执行的命令     
ExecReload=/usr/local/mysql/support-files/mysql.server reload

#停止服务时执行的命令
ExecStop=/usr/local/mysql/support-files/mysql.server stop

#是否分配独立空间   
PrivateTmp=true                                                                         

[Install]
#表示多用户命令行状态
WantedBy=multi-user.target 
EOF
	echo -e "\033[32m mysqld.service 配置成功\033[0m"
	systemctl daemon-reload
fi
sed -i '46s#basedir=#basedir="/usr/local/mysql/"#' /usr/local/mysql/support-files/mysql.server
sed -i '47s#datadir=#datadir="/usr/local/mysql/data/"#' /usr/local/mysql/support-files/mysql.server
sed -i '63s#mysqld_pid_file_path=#mysqld_pid_file_path="/usr/local/mysql/pid/mysql.pid"#' /usr/local/mysql/support-files/mysql.server

echo "八、初始化MySQL数据库"
echo
/usr/local/mysql/scripts/mysql_install_db --basedir=/usr/local/mysql/ --datadir=/usr/local/mysql/data/ --user=mysql &>> /var/log/mysql_install_`date +%F_%Hh`.log
if [ $? -eq 0 ];then

	echo -e "\033[32m Description Initializing The MySQL Database Succeeded ！！！\033[0m"
echo "
MySQL 安装路径：/usr/local/mysql
MySQL 数据库数据文件的存储路径：/usr/local/mysql/data
MySQL mysql.sock存储路径：/usr/local/mysql/sock/mysql.sock
MySQL 指定用户：mysql
MySQL 指定MySQL数据库提供服务的TCP/IP端口：3306
MySQL 默认字符集：utf8mb4
MySQL 默认校对规则：utf8_general_ci
MySQL 支持的字符集：all
MySQL 安装 myisam 存储引擎：1
MySQL 安装 innodb 存储引擎：1
MySQL 安装 memory 存储引擎：1
MySQL 安装 archive 存储引擎：1
MySQL 安装 blackhole 存储引擎：1
MySQL 支持 readline 库：1
MySQL 启用ssl库支持：system
MySQL 启用libz库支持：system
MySQL 支持libwrap库：0
MySQL 使用localmysql客户端的配置：1
MySQL 编译嵌入式服务器支持：1
MySQL 禁用debug：0
MySQL 登陆控制插件：False（默认不开启）
MySQL 密码验证要求插件：False（默认不开启）
MySQL 审计插件：False（默认不开启）
"
else
	echo -e "\033[31m Description Initializing The MySQL Database Failed ！！！\033[0m"
	exit
fi
echo
