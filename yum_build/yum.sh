#!/bin/bash
echo "
################################
#	                       #
#                              #
#    当前脚本适用于Centos7     #
#			       #
#			       #
################################
"
###挂载镜像
IMAGE=`cat /etc/redhat-release | awk -F" " '{print $4}'`
echo "当前系统版本$IMAGE"
echo

umount /dev/cdrom
mount /dev/cdrom /mnt &>/dev/null
if [ $? -eq 0 ];then
	echo "#####挂载镜像成功#####"
	echo
	sleep 2
else
	echo "#####请挂载Centos $IMAGE 镜像#####"
	exit
fi

###ssh 禁止DNS
cat /etc/ssh/sshd_config |grep "UseDNS no" &>/dev/null
if [ $? -ne 0 ];then
	echo "UseDNS no" >> /etc/ssh/sshd_config
	systemctl restart sshd
fi

###构建yum 缓存仓库
mkdir /etc/yum.repos.d/repo &>/dev/null && mv /etc/yum.repos.d/C* repo/ &>/dev/null
if [ $? -eq 0 ];then
	echo "##### /etc/yum.repos.d/repo 目录创建成功#####"
	echo
else
	echo "##### /etc/yum.repos.d/repo 目录已经存在#####"
	echo
fi

cat > /etc/yum.repos.d/local_yum_repo.repo << EOF
[local_yum_repo]
name=local_yum_repo
baseurl=file:///mnt
gpgcheck=0
enable=1
EOF

echo "#####正在构建yum 仓库#####"
echo
yum clean all && yum makecache &>/dev/null
if [ $? -eq 0 ];then
	echo "#####yum 仓库构建成功#####"
	echo
	sleep 2
else
	echo "#####ERROR: YUM 仓库构建失败#####"
	echo "#####请检查本地yum源是否正常或者是否挂载正确的 Centos $IMAGE镜像#####"
	echo
	exit
fi

###安装wget、lrzsz、vim、NetworkManager
yum -y install wget lrzsz vim NetworkManager &>/dev/null
if [ $? -eq 0 ];then
	echo "#####正在安装wget、lrzsz、vim、NetworkManager命令#####"
	echo
	sleep 2
fi

###添加DNS
IPADD=`ip a | grep "2:" | awk -F":" '{print $2}'| awk -F" " '{print $1}'`
cat /etc/sysconfig/network-scripts/ifcfg-$IPADD | grep "DNS" &>/dev/null
if [ $? -eq 0 ];then
	echo "#####DNS已经配置#####"
	echo
	sleep 2
else
	echo "DNS1=8.8.8.8" >> /etc/sysconfig/network-scripts/ifcfg-$IPADD
fi

###关闭NetworkManager
systemctl stop NetworkManager && systemctl disable NetworkManager &>/dev/null
echo "##### NetworkManager 已经关闭#####"
echo
sleep 2

###关闭 Firewalld 防火墙和SElinux
systemctl stop firewalld && systemctl disable firewalld &>/dev/null
echo "#####防火墙已经关闭#####"
echo
sed -i 's#SELINUX=enforcing#SELINUX=disabled#g' /etc/selinux/config
echo "#####SElinux已经关闭#####"
echo

###重启网卡
systemctl restart network
if [ $? -eq 0 ];then
	echo "#####正在重启网卡#####"
	echo
	sleep 2
else
	ifdown $IPADD;ifup $IPADD
fi
	
###判断是否与外网通信
ping -c 3 www.qq.com &>/dev/null

if [ $? -eq 0 ];then
	echo "#####当前网络可以访问外网#####"
	echo
	sleep 2
else
	echo "#####ERROR: 当前网络无法访问外网，请检查当前配置#####"
	exit
fi

###获取阿里的镜像源和epel源
wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo && wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
if [ $? -eq 0 ];then
	echo "#####镜像源已经获取#####"
	echo
	sleep 2
else
	echo "#####镜像源无法获取，请检查是否与外网通信#####"
	exit
fi

###重新构建yum 缓存仓库
echo "#####正在重新构建yum 缓存仓库#####"
echo
sleep 2
yum clean all && yum makecache
